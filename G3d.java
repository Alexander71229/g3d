import javax.sound.midi.*;
import java.io.*;
import java.util.*;
public class G3d{//Tempo constante
	public static long ticksMilisegundos(long res,long tick){
		//return(long)Math.round(tick*(480./res))+200;//60000/125=
		return(long)Math.round(tick*(480./res));//60000/125=
	}
	public static void main(String[]args){
		try{
			String nombre="Du Hast";
			PrintStream ps=new PrintStream(new FileOutputStream(new File(nombre+".js")));
			File f=new File(nombre+".mid");
			Sequence s=MidiSystem.getSequence(f);
			long res=s.getResolution();
			Track t=null;
			MidiEvent me=null;
			double[]pss=new double[128];
			boolean[]pst=new boolean[128];
			ps.println("evs=[];");
      for(int i=0;i<s.getTracks().length;i++){
        t=s.getTracks()[i];
        for(int j=0;j<t.size();j++){
          me=t.get(j);
					if(me.getMessage().getStatus()>=0x90&&me.getMessage().getStatus()<=0x9F){
						if(!pst[me.getMessage().getMessage()[1]]){
							pst[me.getMessage().getMessage()[1]]=true;
							pss[me.getMessage().getMessage()[1]]=ticksMilisegundos(res,me.getTick());
						}
					}
					if(me.getMessage().getStatus()>=0x80&&me.getMessage().getStatus()<=0x8F){
						if(pst[me.getMessage().getMessage()[1]]){
							pst[me.getMessage().getMessage()[1]]=false;
							ps.println("evs.push(new Evento("+pss[me.getMessage().getMessage()[1]]+","+ticksMilisegundos(res,me.getTick())+",0,0,0,0,0,'',"+i+",1,"+me.getMessage().getMessage()[1]+"));");
						}
					}
        }
      }
		}catch(Throwable t){
			t.printStackTrace();
		}
	}
}