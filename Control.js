function Control(o){
	this.tini=0;
	this.ini=0;
	this.r=1;
	this.p=0;
	this.at=-1;
	this.o=o;
	this.ostd=1;
	this.std=0;
	this.tpu=0;
	this.tpi=(new Date()).getTime();
	this.vl=[];
	this.tmx=0;
	this.getTime=function(f){
		var t=0;
		if(this.std==1){
			if(this.o&&this.ostd&&!f){
				t=o.getTime();
				if(t!=this.at){
					this.tini=(new Date()).getTime();
				}
				this.p=((new Date()).getTime()-this.tini)*this.r+t;
				this.at=t;
			}else{
				this.p=((new Date()).getTime()-this.tpi)*this.r+this.tpu;
			}
		}
		if(this.p>0&&this.ostd==0){
			this.habilitar(true);
		}
		return this.p;
	}
	this.setTime=function(p,f){
		if(p<0||p>this.o.getLength){
			this.habilitar(false);
		}else{
			this.habilitar(true);
		}
		if(this.o&&this.ostd)this.o.setTime(p);
		this.p=p;
		this.tpu=this.p;
		this.tpi=(new Date()).getTime();
		if(f){
			this.tpi=this.o.getTini();
		}
	}
	this.setRate=function(r,f){
		if(r<0){
			return;
		}
		this.tpu=this.p;
		this.tpi=(new Date()).getTime();
		if(this.o&&this.ostd)this.o.setRate(r);
		this.r=r;
		if(f){
			this.tpi=this.o.getTini();
		}
	}
	this.play=function(f){
		this.tpu=this.p;
		this.tpi=(new Date()).getTime();
		if(this.o&&this.ostd){
			this.o.setRate(this.r);
			this.o.play();
			if(f){
				this.o.setTime(this.p);
				this.tpi=this.o.getTini();
			}
		}
		this.std=1;
	}
	this.stop=function(f){
		this.tpu=this.p;
		this.tpi=(new Date()).getTime();
		if(this.o&&this.ostd)this.o.stop();
		this.std=0;
		if(f){
			this.tpi=this.o.getTini();
		}
	}
	this.addTime=function(dt,f){
		this.setTime(this.p+dt,f);
	}
	this.addRate=function(dt,f){
		this.setRate(this.r+dt,f);
	}
	this.habilitar=function(op){
		if(op){
			this.ostd=1;
			if(this.std==1){
				if(this.o){
					this.o.setTime(this.p);
					this.o.play();
					this.o.setRate(this.r);
				}
			}
		}else{
			this.ostd=0;
			if(this.o){
				this.o.stop();
			}
		}
	}
	this.getPos=function(){
		return this.p/this.tmx;
	}
	this.setPos=function(p,f){
		this.setTime(this.tmx*p,f);
	}
}
