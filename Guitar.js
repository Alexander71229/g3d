function fxc(a,b,t){
	return a*(1-t)+b*t;
}
function line(ctx,x1,y1,x2,y2,c){
	ctx.strokeStyle=c||"white";
	ctx.beginPath();
	ctx.moveTo(x1,y1);
	ctx.lineTo(x2,y2);
	ctx.stroke();
}
function Guitar(){
	this.c=[64,59,55,50,45,40];
	this.n=21;
	this.psx1=[];
	this.psx2=[];
	this.mxc=function(n){
		for(var i=0;i<this.c.length;i++){
			if(n>=this.c[i]){
				return[i,n-this.c[i]];
			}
		}
	}
	this.mf=function(n,fi,ff){
		for(var i=0;i<this.c.length;i++){
			if(n>=this.c[i]&&n-this.c[i]>=fi&&n-this.c[i]<=ff){
				return[i,n-this.c[i]];
			}
		}
	}
	this.dibIni=function(ctx,x1,y1,w1,h1,x2,y2,w2,h2){
		this.x1=x1;
		this.y1=y1;
		this.w1=w1;
		this.h1=h1;
		this.x2=x2;
		this.y2=y2;
		this.w2=w2;
		this.h2=h2;
		this.l1=w1/(1-1/(Math.pow(2,(this.n/12))));
		this.l2=w2/(1-1/(Math.pow(2,(this.n/12))));
		this.dh1=h1/this.c.length;
		this.dh2=h2/this.c.length;
		for(var i=0;i<=this.n;i++){
			var x=this.x1+this.l1*(1-1/Math.pow(2,i/12.));
			this.psx1.push(x);
		}
		for(var i=0;i<=this.n;i++){
			var x=this.x2+this.l2*(1-1/Math.pow(2,i/12.));
			this.psx2.push(x);
		}
	}
	this.dg1=function(){
		for(var i=0;i<=5;i++){
			var y=this.y1+this.dh1*(i+.5);
			line(ctx,this.x1,y,this.x1+this.w1,y);
		}
		for(var i=0;i<=this.n;i++){
			var x=this.psx1[i];
			line(ctx,x,this.y1,x,this.y1+this.h1);
		}
	}
	this.dcs=function(){
		for(var i=0;i<=this.n;i++){
			line(ctx,this.psx1[i],this.y1+this.h1,this.psx2[i],this.y2+this.h2,'#161616');
		}
		for(var i=0;i<this.c.length;i++){
			line(ctx,this.x1,this.y1+this.dh1/2+this.dh1*i,this.x2,this.y2+this.dh2/2+this.dh2*i,'#161616');
		}
		for(var i=0;i<this.c.length;i++){
			line(ctx,this.x1+this.w1,this.y1+this.dh1/2+this.dh1*i,this.x2+this.w2,this.y2+this.dh2/2+this.dh2*i,'#161616');
		}
	}
	this.dg2=function(){
		for(var i=0;i<=5;i++){
			var y=this.y2+this.dh2*(i+.5);
			line(ctx,this.x2,y,this.x2+this.w2,y);
		}
		for(var i=0;i<=this.n;i++){
			var x=this.psx2[i];
			line(ctx,x,this.y2,x,this.y2+this.h2);
		}
		var gtx=[3,5,7,9,12,15,17,19,21];
		for(var i=0;i<gtx.length;i++){
			line(ctx,fxc(this.psx2[gtx[i]-1],this.psx2[gtx[i]],.2),this.y2+this.h2/2-this.dh2/10,fxc(this.psx2[gtx[i]-1],this.psx2[gtx[i]],.8),this.y2+this.h2/2+this.dh2/10);
		}
	}
	this.dib=function(){
		this.dg1();
		this.dcs();
		this.dg2();
	}
	this.Render=function(m,w,h,g){
		this.g=g;
		this.m=m;
		this.w=w;
		this.h=h;
		this.gs={};
		this.colores=["#FF0000","#00FF00","#0000FF","#FFFF00","#FF00FF","#00FFFF"];
		this.coloresSombra=["#800000","#008000","#000080","#808000","#800080","#008080"];
		this.add=function(tr){
			if(e.length){
				for(var i=0;i<e.length;i++){
					this.gs[e[i].id]=e[i];	
				}
			}else{
				this.gs[e.id]=e;			
			}
		}
		this.u=function(t,tm){
			if(!tm){
				alert("Error en Render.u falta tm" );
				return;
			}
			for(var i in this.gs){
				this.gra(this.gs[i],t,tm);
			}
		}
		this.mostrar=function(c1,c2){
		
		}
		this.rem=function(e){
			if(this.gs[e.id]){
				delete this.gs[e.id];
			}
		}
		this.pre=function(e,t,tm){
			e.y2=(e.tfin-t)/tm;
			e.y1=(e.tini-t)/tm;
			if(e.y1<0){
				e.y1=0;
			}
			if(e.y2>1){
				e.y2=1;
			}
		}
		this.vis=function(e){
			return(!(e.y1>1||e.y2<0||e.x1>1||e.x2<0))&&(e.n1>=40&&e.n1<=85);
		}
		this.dibe=function(e){
			if(!this.m.ts[e.t].v){
				return;
			}
			if(e.n3==0){
				var qa=1-.5;
				var qb=1-qa;
				var mx1a=e.y1*(this.g.x1)+(1-e.y1)*(this.g.x2);
				var mx1b=e.y1*(this.g.x1+this.g.w1)+(1-e.y1)*(this.g.x2+this.g.w2);
				var mx2a=e.y2*(this.g.x1)+(1-e.y2)*(this.g.x2);
				var mx2b=e.y2*(this.g.x1+this.g.w1)+(1-e.y2)*(this.g.x2+this.g.w2);
				var my1a=e.y1*(qa*(this.g.y1+this.g.dh1*e.n2)+qb*(this.g.y1+this.g.dh1*(e.n2+1)))+(1-e.y1)*(qa*(this.g.y2+this.g.dh2*e.n2)+qb*(this.g.y2+this.g.dh2*(e.n2+1)));
				var my1b=e.y1*(qb*(this.g.y1+this.g.dh1*e.n2)+qa*(this.g.y1+this.g.dh1*(e.n2+1)))+(1-e.y1)*(qb*(this.g.y2+this.g.dh2*e.n2)+qa*(this.g.y2+this.g.dh2*(e.n2+1)));
				var my2a=e.y2*(qa*(this.g.y1+this.g.dh1*e.n2)+qb*(this.g.y1+this.g.dh1*(e.n2+1)))+(1-e.y2)*(qa*(this.g.y2+this.g.dh2*e.n2)+qb*(this.g.y2+this.g.dh2*(e.n2+1)));
				var my2b=e.y2*(qb*(this.g.y1+this.g.dh1*e.n2)+qa*(this.g.y1+this.g.dh1*(e.n2+1)))+(1-e.y2)*(qb*(this.g.y2+this.g.dh2*e.n2)+qa*(this.g.y2+this.g.dh2*(e.n2+1)));
				//Sombras
				line(ctx,fxc(this.g.x2,this.g.x1,e.y1),fxc(this.g.y2+this.g.h2,this.g.y1+this.g.h1,e.y1),fxc(this.g.x2+this.g.w2,this.g.x1+this.g.w1,e.y1),fxc(this.g.y2+this.g.h2,this.g.y1+this.g.h1,e.y1),this.coloresSombra[e.n2]);
			}else{
				var qa=1-.3;
				var qb=1-qa;
				var mx1a=e.y1*(qa*this.g.psx1[e.n3-1]+qb*this.g.psx1[e.n3])+(1-e.y1)*(qa*this.g.psx2[e.n3-1]+qb*this.g.psx2[e.n3]);
				var mx1b=e.y1*(qb*this.g.psx1[e.n3-1]+qa*this.g.psx1[e.n3])+(1-e.y1)*(qb*this.g.psx2[e.n3-1]+qa*this.g.psx2[e.n3]);
				var mx2a=e.y2*(qa*this.g.psx1[e.n3-1]+qb*this.g.psx1[e.n3])+(1-e.y2)*(qa*this.g.psx2[e.n3-1]+qb*this.g.psx2[e.n3]);
				var mx2b=e.y2*(qb*this.g.psx1[e.n3-1]+qa*this.g.psx1[e.n3])+(1-e.y2)*(qb*this.g.psx2[e.n3-1]+qa*this.g.psx2[e.n3]);
				var my1a=e.y1*(qa*(this.g.y1+this.g.dh1*e.n2)+qb*(this.g.y1+this.g.dh1*(e.n2+1)))+(1-e.y1)*(qa*(this.g.y2+this.g.dh2*e.n2)+qb*(this.g.y2+this.g.dh2*(e.n2+1)));
				var my1b=e.y1*(qb*(this.g.y1+this.g.dh1*e.n2)+qa*(this.g.y1+this.g.dh1*(e.n2+1)))+(1-e.y1)*(qb*(this.g.y2+this.g.dh2*e.n2)+qa*(this.g.y2+this.g.dh2*(e.n2+1)));
				var my2a=e.y2*(qa*(this.g.y1+this.g.dh1*e.n2)+qb*(this.g.y1+this.g.dh1*(e.n2+1)))+(1-e.y2)*(qa*(this.g.y2+this.g.dh2*e.n2)+qb*(this.g.y2+this.g.dh2*(e.n2+1)));
				var my2b=e.y2*(qb*(this.g.y1+this.g.dh1*e.n2)+qa*(this.g.y1+this.g.dh1*(e.n2+1)))+(1-e.y2)*(qb*(this.g.y2+this.g.dh2*e.n2)+qa*(this.g.y2+this.g.dh2*(e.n2+1)));
				if(e.y1>0){
					//Sombras
					line(ctx,qa*this.g.psx2[e.n3-1]+qb*this.g.psx2[e.n3],this.g.y2+qa*this.g.dh2*e.n2+qb*this.g.dh2*(e.n2+1),qb*this.g.psx2[e.n3-1]+qa*this.g.psx2[e.n3],this.g.y2+qb*this.g.dh2*e.n2+qa*this.g.dh2*(e.n2+1),this.coloresSombra[e.n2]);
					//line(ctx,fxc(this.g.x2,this.g.x1,e.y1),fxc(this.g.y2+this.g.dh2*e.n2+this.g.dh2*qb,this.g.y1+this.g.dh1*e.n2+this.g.dh1*qb,e.y1),fxc(this.g.x2,this.g.x1,e.y1),fxc(this.g.y2+this.g.dh2*e.n2+this.g.dh2*qa,this.g.y1+this.g.dh1*e.n2+this.g.dh1*qa,e.y1),c);
					line(ctx,fxc(this.g.x2,this.g.x1,e.y1),fxc(this.g.y2+this.g.dh2*e.n2,this.g.y1+this.g.dh1*e.n2,e.y1),fxc(this.g.x2,this.g.x1,e.y1),fxc(this.g.y2+this.g.dh2*(e.n2+1),this.g.y1+this.g.dh1*(e.n2+1),e.y1),this.coloresSombra[e.n2]);
					line(ctx,fxc(this.g.psx2[e.n3-1],this.g.psx1[e.n3-1],e.y1),fxc(this.g.y2+this.g.h2,this.g.y1+this.g.h1,e.y1),fxc(this.g.psx2[e.n3],this.g.psx1[e.n3],e.y1),fxc(this.g.y2+this.g.h2,this.g.y1+this.g.h1,e.y1),this.coloresSombra[e.n2]);
					line(ctx,fxc((this.g.psx2[e.n3-1]+this.g.psx2[e.n3])/2.,(this.g.psx1[e.n3-1]+this.g.psx1[e.n3])/2.,e.y1),fxc(this.g.y2+this.g.dh2*e.n2+this.g.dh2*qa,this.g.y1+this.g.dh1*e.n2+this.g.dh1*qa,e.y1),fxc((this.g.psx2[e.n3-1]+this.g.psx2[e.n3])/2.,(this.g.psx1[e.n3-1]+this.g.psx1[e.n3])/2.,e.y1),fxc(this.g.y2+this.g.h2,this.g.y1+this.g.h1,e.y1),this.coloresSombra[e.n2]);
					line(ctx,fxc(this.g.x2,this.g.x1,e.y1),fxc(this.g.y2+this.g.dh2*(e.n2+.5),this.g.y1+this.g.dh1*(e.n2+.5),e.y1),fxc(qa*this.g.psx2[e.n3-1]+qb*this.g.psx2[e.n3],qa*this.g.psx1[e.n3-1]+qb*this.g.psx1[e.n3],e.y1),fxc(this.g.y2+this.g.dh2*(e.n2+.5),this.g.y1+this.g.dh1*(e.n2+.5),e.y1),this.coloresSombra[e.n2]);
				}
			}
			line(ctx,mx2a,my2a,mx2b,my2a,this.colores[e.n2]);
			line(ctx,mx2b,my2a,mx2b,my2b,this.colores[e.n2]);
			line(ctx,mx2b,my2b,mx2a,my2b,this.colores[e.n2]);
			line(ctx,mx2a,my2b,mx2a,my2a,this.colores[e.n2]);
			line(ctx,mx1a,my1a,mx2a,my2a,this.colores[e.n2]);
			line(ctx,mx1b,my1a,mx2b,my2a,this.colores[e.n2]);
			line(ctx,mx1b,my1b,mx2b,my2b,this.colores[e.n2]);
			line(ctx,mx1a,my1b,mx2a,my2b,this.colores[e.n2]);
			line(ctx,mx1a,my1a,mx1b,my1a,this.colores[e.n2]);
			line(ctx,mx1b,my1a,mx1b,my1b,this.colores[e.n2]);
			line(ctx,mx1b,my1b,mx1a,my1b,this.colores[e.n2]);
			line(ctx,mx1a,my1b,mx1a,my1a,this.colores[e.n2]);
		}
		this.gra=function(e,t,tm){
			this.pre(e,t,tm);
			if(this.vis(e)){
				if(e.i==1){
					this.dibe(e);
				}
			}else{
				this.rem(e);
			}
		}
		this.add=function(e){
			if(e.length){
				for(var i=0;i<e.length;i++){
					this.gs[e[i].id]=e[i];	
				}
			}else{
				this.gs[e.id]=e;			
			}
		}
		this.u=function(t,tm){
			if(!tm){
				alert("Error en Render.u falta tm" );
				return;
			}
			ctx.clearRect(0,0,this.w,this.h);
			ctx.fillRect(0,0,this.w,this.h);
			this.g.dg1();
			this.g.dcs();
			for(var i in this.gs){
				this.gra(this.gs[i],t,tm);
			}
			this.g.dg2();
		}
	}
}
guitar=new Guitar();