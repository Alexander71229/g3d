function H5(o){
  this.getTime=function(){
  	return o.currentTime*1000;
  }
  this.setTime=function(t){
  	o.currentTime=t/1000.;
  }
  this.getRate=function(){
  	return o.playbackRate;
  }
  this.setRate=function(r){
  	o.playbackRate=r;
  }
  this.play=function(){
  	o.play();
  }
  this.stop=function(){
  	o.pause();
  }
  this.getLength=function(){
  	return o.duration;
  }
}