import javax.sound.midi.*;
import java.io.*;
import java.util.*;
public class Escribir{//Tempo constante
	public static MTempo mt;
	public static long ticksMilisegundos(long res,long tick){
		//return(long)Math.round(tick*(480./res))+200;//60000/125=
		//return(long)Math.round(tick*(480./res));//60000/125=
		return mt.tiempo(tick)/1000;
	}
	public static void escribir(String nombre,Sequence s)throws Exception{
		PrintStream ps=new PrintStream(new FileOutputStream(new File(nombre+".js")));
		long res=s.getResolution();
		mt=new MTempo(res);
		Track t=null;
		MidiEvent me=null;
		HashMap<String,Long>pss=new HashMap<String,Long>();
		HashMap<String,Boolean>pst=new HashMap<String,Boolean>();
		ArrayList<Evento>[]canales=(ArrayList<Evento>[])new ArrayList[16];
		ps.println("evs=[];");
		for(int i=0;i<s.getTracks().length;i++){
			t=s.getTracks()[i];
			for(int j=0;j<t.size();j++){
				me=t.get(j);
				mt.add(me);
			}
		}
		for(int i=0;i<s.getTracks().length;i++){
			t=s.getTracks()[i];
			for(int j=0;j<t.size();j++){
				me=t.get(j);
				ShortMessage sm=null;
				try{
					sm=(ShortMessage)me.getMessage();
				}catch(Exception e){
				}
				if(sm!=null&&sm.getChannel()!=9){
					if(me.getMessage().getStatus()>=0x90&&me.getMessage().getStatus()<=0x9F&&me.getMessage().getMessage().length>=2&&me.getMessage().getMessage()[1]>=0&&me.getMessage().getMessage()[1]<=128&&sm.getData2()>0){
						if(pst.get(sm.getChannel()+":"+me.getMessage().getMessage()[1])==null||!pst.get(sm.getChannel()+":"+me.getMessage().getMessage()[1])){
							pst.put(sm.getChannel()+":"+me.getMessage().getMessage()[1],true);
							pss.put(sm.getChannel()+":"+me.getMessage().getMessage()[1],ticksMilisegundos(res,me.getTick()));
						}
					}
					if(((me.getMessage().getStatus()>=0x80&&me.getMessage().getStatus()<=0x8F)||(me.getMessage().getStatus()>=0x90&&me.getMessage().getStatus()<=0x9F&&sm.getData2()==0))&&me.getMessage().getMessage().length>=2&&me.getMessage().getMessage()[1]>=0&&me.getMessage().getMessage()[1]<=128){
						if(pst.get(sm.getChannel()+":"+me.getMessage().getMessage()[1])!=null&&pst.get(sm.getChannel()+":"+me.getMessage().getMessage()[1])){
							pst.put(sm.getChannel()+":"+me.getMessage().getMessage()[1],false);
							//ps.println("evs.push(new Evento("+pss[me.getMessage().getMessage()[1]]+","+ticksMilisegundos(res,me.getTick())+",0,0,0,0,0,'',"+i+",1,"+me.getMessage().getMessage()[1]+"));");
							//ps.println("evs.push(new Evento("+pss[me.getMessage().getMessage()[1]]+","+ticksMilisegundos(res,me.getTick())+",0,0,0,0,0,'',"+sm.getChannel()+",1,"+me.getMessage().getMessage()[1]+"));");
							if(canales[sm.getChannel()]==null){
								canales[sm.getChannel()]=new ArrayList<Evento>();
							}
							canales[sm.getChannel()].add(new Evento(pss.get(sm.getChannel()+":"+me.getMessage().getMessage()[1]),ticksMilisegundos(res,me.getTick()),sm.getChannel()*100+i,me.getMessage().getMessage()[1]));
						}
					}
				}
			}
		}
		for(int i=0;i<canales.length;i++){
			if(canales[i]!=null){
				for(int j=0;j<canales[i].size();j++){
					ps.println(canales[i].get(j)+"");
				}
			}
		}
	}
}