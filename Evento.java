public class Evento{
	public long inicio;
	public long duracion;
	public int canal;
	public int nota;
	public Evento(long inicio,long duracion,int canal,int nota){
		this.inicio=inicio;
		this.duracion=duracion;
		this.canal=canal;
		this.nota=nota;
	}
	public String toString(){
		return "evs.push(new Evento("+this.inicio+","+this.duracion+",0,0,0,0,0,'',"+this.canal+",1,"+this.nota+"));";
	}
}