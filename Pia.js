function rect(x,y,w,h,c,b){
  var div=document.createElement("div");
  div.style.position='absolute';
  div.style.top=y;
  div.style.left=x;
	div.style.width=w;
	div.style.height=h;
	div.style.backgroundColor=c;
	div.style.border='1px solid '+b;
	document.body.appendChild(div);
	return div;
}

function Pia(ni,nf,e,n,b){
  this.e=e||163;
  this.n=n||167;
  this.b=b||285;
  this.ni=ni||36;
  this.nf=nf||96;
  this.pos=function(o){
    var res={};
    var p=o%12;
    var u=0;
    var n=this.n;
    var e=this.e;
    var b=this.b;
    if(p<=4){
      if(p%2==0){
      	var t=p/2;
      }else{
      	u=3*b/2-(2*n+e)/2;
      	var t=(p-1)/2;
      }
    }else{
      if(p%2==1){
      	var t=(p+1)/2;
      }else{
      	u=5*b-(3*n+2*e)/2;
      	var t=(p-6)/2;
      }
    }
    if(u==0){
			res.p=b*t;
     	res.t=b;
     	res.o=0;
    }else{
    	res.p=(n+e)*t+u;
     	res.t=n;
     	res.o=1;
    }
    res.p=res.p+(o-p)*(7*b)/12;
    return res;
  }
  this.posr=function(o){
		var res=this.pos(o);
		res.p=(res.p-this.xi.p)/(this.xf.p-this.xi.p);
		res.t=res.t/(this.xf.p-this.xi.p);
		return res;
  }
  this.dib=function(x1,y1,w,h,p){
  	x1=x1||0;
  	y1=y1||0;
		h=h||120;
		w=w||300;
		p=p||0.6;
		var ni=this.ni;
		var nf=this.nf;

		for(i=ni;i<nf;i++){
			var ps=this.posr(i);
			if(ps.o==0){
				d=rect(ps.p*w+x1,y1,ps.t*w,h,'white','black');
				d.style.zIndex=9999998;
			}
			if(ps.o==1){
				d=rect(ps.p*w+x1,y1,ps.t*w,h*p,'black','black');
				d.style.zIndex=9999999;
			}
		}
  }
  this.xi=this.pos(this.ni);
  this.xf=this.pos(this.nf);
}
