package utilidades;
import java.io.*;
import java.util.*;
import java.text.*;
public class General{
	public static PrintStream ps=null;
	public static long clave;
	public static String ruta=null;
	public static String archivo=null;
	private static PrintStream reg=null;
	private static HashMap<String,Integer>hash;
	public static long calcularClave(long time){
		return (long)Math.floor((time-18000000)/86400000.);
	}
	public static String obtenerNombre(Date date){
		SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMdd");
		return sdf.format(date);
	}
	public static String rutaClase(Class c)throws Exception{
		String rutax=c.getName();
		File f=new File(c.getResource("/"+rutax.replaceAll("\\.","/")+".class").getFile());
		return f.getCanonicalPath();
	}
	public static String obtenerRutaCalculada(){
		String ruta="/";
		try{
			Class c=General.class;
			String rutax=c.getName();
			File f=new File(c.getResource("/"+rutax.replaceAll("\\.","/")+".class").getFile());
			while(f!=null){
				if(f.getName().equals("WEB-INF")){
					f=f.getParentFile();
					break;
				}
				f=f.getParentFile();
			}
			ruta=f.getCanonicalPath()+File.separator;
		}catch(Throwable t){
			ruta="";
		}
		return ruta;
	}
	public static void calcularRuta(){
		ruta=obtenerRutaCalculada();
	}
	public static void imprimir(String mensaje){
		try{
			obtener();
			Date tiempoActual=new Date();
			ps.println(formato(tiempoActual)+":"+(tiempoActual).getTime()+":"+mensaje);
			System.out.println(formato(tiempoActual)+":"+(tiempoActual).getTime()+":"+mensaje);
		}catch(Exception e){
			e.printStackTrace();
			clave=0;
		}
	}
	public static void imprimir(Throwable t){
		try{
			obtener();
			Date tiempoActual=new Date();
			ps.println(formato(tiempoActual)+":"+(tiempoActual).getTime());
			System.out.println(formato(tiempoActual)+":"+(tiempoActual).getTime());
			t.printStackTrace(ps);
			t.printStackTrace();
			registrar(t);
		}catch(Exception e){
			clave=0;
		}
	}
	public static void imprimir(Throwable t,String mensaje){
		try{
			obtener();
			Date tiempoActual=new Date();
			ps.println(formato(tiempoActual)+":"+(tiempoActual).getTime()+":"+mensaje);
			System.out.println(formato(tiempoActual)+":"+(tiempoActual).getTime()+":"+mensaje);
			t.printStackTrace(ps);
			t.printStackTrace();
			registrar(t);
		}catch(Exception e){
			clave=0;
		}
	}
	public static void registrar(Throwable t){
		try{
			if(reg==null){
				reg=new PrintStream(new FileOutputStream(new File(ruta+"registro.txt"),true));
			}
			String clave=t.getStackTrace()[0].getClassName()+":"+t.getStackTrace()[0].getLineNumber()+"->"+t;
			if(hash==null){
				hash=new HashMap<String,Integer>();
			}
			if(hash.get(clave)==null){
				Date tiempoActual=new Date();
				reg.println(formato(tiempoActual)+":"+(tiempoActual).getTime()+":Excepci�n "+(hash.size())+":Clave:"+clave);
				t.printStackTrace(reg);
				hash.put(clave,new Integer(1));
				if(hash.size()>1000){
					hash=null;
					reg.println("M�s de mil errores");
				}
			}
		}catch(Throwable tx){
			reg=null;
		}
	}
	public static String formato(Date date){
		SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMdd HH:mm:ss");
		return sdf.format(date);
	}
	private static void obtener(){
		Date d=new Date();
		long time=d.getTime();
		long nuevaClave=calcularClave(time);
		if(clave!=nuevaClave||ps==null){
			try{
				if(ruta==null){
					calcularRuta();
				}
				String nombre=obtenerNombre(d);
				if(!ruta.endsWith(File.separator)){
					ruta+=File.separator;
				}
				File f=new File(ruta+nombre+".txt");
				archivo=f.getCanonicalPath();
				System.out.println("Archivo de log:"+f.getCanonicalPath());
				ps=new PrintStream(new FileOutputStream(f,true));
				clave=nuevaClave;
				imprimir(clave+":"+rutaClase(General.class)+" INICIO LOG:"+f.getCanonicalPath());
			}catch(Exception e){
			}
		}
	}
	public static String traza(int a){
		String cad="";
		try{
			throw new Exception();
		}catch(Exception e){
			try{
				String s="";
				for(int i=1+a;i<e.getStackTrace().length;i++){
					cad+=s+e.getStackTrace()[i].getClassName()+"."+e.getStackTrace()[i].getMethodName()+":"+e.getStackTrace()[i].getLineNumber();
					s=" ";
				}
			}catch(Throwable t){
			}
		}
		return cad;
	}
	public static void traza(){
		imprimir(traza(1));
	}
	public static void traza(String m){
		imprimir(traza(1)+"->"+m);
	}
	public static String jar(String nombreClase)throws Exception{
		Class clase=Class.forName(nombreClase);
		return clase.getResource("/"+nombreClase.replaceAll("\\.","/")+".class")+"";
	}
}
