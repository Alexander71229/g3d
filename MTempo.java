import java.util.*;
import javax.sound.midi.*;
public class MTempo{
	public ArrayList<Tempo>tmps;
	public double res;
	public MTempo(double res){
		tmps=new ArrayList<Tempo>();
		this.res=res;
	}
	public void add(MidiEvent e){
		Tempo tempo=Tempo.getTempo(e);
		if(tempo!=null){
			tempo.tmil=tiempo(e.getTick());
			tmps.add(tempo);
		}
	}
	public long tiempo(long tick){
		long resultado=0;
		Tempo tempo=null;
		double val=500000.;
		double tmil=0;
		long tck=0;
		for(int i=0;i<tmps.size();i++){
			if(tmps.get(i).tck<tick){
				if(tempo==null||tmps.get(i).tck>tempo.tck){
					tempo=tmps.get(i);
				}
			}
		}
		if(tempo!=null){
			val=tempo.val;
			tmil=tempo.tmil;
			tck=tempo.tck;
		}
		resultado=Math.round(tmil+(tick-tck)/res*val);
		return resultado;
	}
}