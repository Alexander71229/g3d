function Render(m,w,h,x1,y1){
  this.x1=x1||0;
  this.y1=y1||0;
	this.h=h||480;
	this.w=w||640;
	this.m=m;
	this.f=1;
	if(window.Pia){
		this.p=new Pia();
	}
	if(this.p){
		this.p.dib(this.x1,this.h,this.w,50);
	}
	this.gs={};
	this.z=3;
	this.mov=function(e){
		/*if(this.m.ts[e.t]){
			e.v=this.m.ts[e.t].v;
		}*/
		if(e.v&&this.m.ts[e.t].v){
			var x1=e.x1;
			var y1=e.y1;
			var x2=e.x2;
			var y2=e.y2;
			e.div.style.width=(x2-x1)*this.w-1;
			e.div.style.height=(y2-y1)*this.h-1;
			e.div.style.left=x1*this.w+this.x1;
			e.div.style.top=(1-y2)*this.h+this.y1;
			e.div.style.display='';
			e.div.style.zIndex=this.z;
			this.z++;
		}else{
			e.div.style.display='none';
		}
	}
	this.rem=function(e){
		if(e.div){
			document.body.removeChild(e.div);
			delete e.div;
		}
		if(this.gs[e.id]){
			delete this.gs[e.id];
		}
	}
	this.rec=function(c1,c2,e){
		c1=c1||'blue';
		c2=c2||'red';
		e=e||'';
		var div=document.createElement('div');
		div.style.position='absolute';
		div.style.border='1px solid '+c1;
		div.style.backgroundColor=c2;
		div.style.display='none';
		div.innerHTML="<table width='100%' height='100%'><tr><td valign='bottom'>"+e+"</td></tr></table>";
		document.body.appendChild(div);
		return div;
	}
	this.pre=function(e,t,tm){
		e.y2=(e.tfin-t)/tm;
		e.y1=(e.tini-t)/tm;
	}
	this.vis=function(e){
		return !(e.y1>1||e.y2<0||e.x1>1||e.x2<0);
	}
	this.gra=function(e,t,tm){
		this.pre(e,t,tm);
		if(this.vis(e)){
			if(!e.div){
				if(this.m.ts[e.t]){
					e.c1=e.c1||this.m.ts[e.t].c1;
					e.c2=e.c2||this.m.ts[e.t].c2;
				}
				e.div=this.rec(e.c1,e.c2,e.e);
			}
			if(e.i==1){
				if(e.n1){
					if(this.p){
						var ps=this.p.posr(e.n1);
						e.x1=ps.p;
						e.x2=e.x1+ps.t;
					}
				}
			}
			this.mov(e);
		}else{
			this.rem(e);
		}
	}
	this.add=function(e){
		if(e.length){
			for(var i=0;i<e.length;i++){
				this.gs[e[i].id]=e[i];	
			}
		}else{
			this.gs[e.id]=e;			
		}
	}
	this.u=function(t,tm){
		if(!tm){
			alert("Error en Render.u falta tm" );
			return;
		}
		for(var i in this.gs){
			this.gra(this.gs[i],t,tm);
		}
	}
	this.mostrar=function(c1,c2){
		c1=c1||'black';
		c2=c2||'white';
		var div=document.createElement('div');
		div.style.position='absolute';
		div.style.border='1px solid '+c1;
		div.style.backgroundColor=c2;
		div.style.left=this.x1;
		div.style.top=this.y1;
		div.style.width=this.w;
		div.style.height=this.h;
		div.style.zIndex=1;
		div.style.verticalAlign="bottom";
		this.div=div;
		document.body.appendChild(div);
	}
}