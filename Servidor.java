import com.sun.net.httpserver.*;
import java.io.*;
import java.util.*;
import java.net.*;
import java.nio.file.*;
import org.apache.commons.fileupload.*;
import javax.sound.midi.*;
public class Servidor{
	public static Sequencer sx;
	public static Map<String,Ejecutable>hash;
	public static long tini=0;
	public static byte[]archivo(String ruta)throws Exception{
		return Files.readAllBytes(Paths.get(ruta));
	}
	public static interface Ejecutable{
		public byte[]ejecutar(HttpExchange httpExchange,Map<String,String>parametros)throws Exception;
	}
	public static class Indice implements Ejecutable{
		public byte[]ejecutar(HttpExchange httpExchange,Map<String,String>parametros)throws Exception{
			return archivo("indice.html");
		}
	}
	public static class Actual implements Ejecutable{
		public byte[]ejecutar(HttpExchange httpExchange,Map<String,String>parametros)throws Exception{
			if(parametros.get("v")!=null){
				return archivo("Piano.html");
			}
			return archivo("Actual.html");
		}
	}
	public static class Control implements Ejecutable{
		public byte[]ejecutar(HttpExchange httpExchange,Map<String,String>parametros)throws Exception{
			String v=parametros.get("v");
			utilidades.General.imprimir(parametros.get("p")+":"+parametros.get("v"));
			if(parametros.get("p").equals("P")){
				play();
			}
			if(parametros.get("p").equals("S")){
				stop();
			}
			if(parametros.get("p").equals("getTime")){
				return(getTime()+"").getBytes();
			}
			if(parametros.get("p").equals("setTime")){
				setTime(Math.round(Double.parseDouble(v)));
			}
			if(parametros.get("p").equals("getRate")){
				return(getRate()+"").getBytes();
			}
			if(parametros.get("p").equals("setRate")){
				setRate(Float.parseFloat(v));
			}
			if(parametros.get("p").equals("getLength")){
				return(getLength()+"").getBytes();
			}
			if(parametros.get("p").equals("t")){
				return (tini+"").getBytes();
			}
			return "Ok".getBytes();
		}
	}
	public static class Subir implements Ejecutable{
		public byte[]ejecutar(final HttpExchange httpExchange,Map<String,String>parametros)throws Exception{
			DiskFileUpload fileUpload=new DiskFileUpload();
			List<FileItem>fileItems=fileUpload.parseRequest(new RequestContext(){
				@Override
				public String getCharacterEncoding(){
					return "UTF-8";
				}
				@Override
				public int getContentLength(){
					return 0;
				}
				@Override
				public String getContentType(){
					return httpExchange.getRequestHeaders().getFirst("Content-type");
				}
				@Override
				public InputStream getInputStream()throws IOException{
					return httpExchange.getRequestBody();
				}
			});
			for(int i=0;i<fileItems.size();i++){
				FileItem fi=fileItems.get(i);
				if(fi.get().length>0){
					abrir(fi.get());
				}
			}
			//return "OK".getBytes();
			return archivo("Actual.html");
		}
	}
	public static void writeResponse(HttpExchange httpExchange,byte[]response)throws IOException{
		httpExchange.sendResponseHeaders(200,response.length);
		OutputStream os=httpExchange.getResponseBody();
		os.write(response);
		os.close();
	}
	public static Map<String,String>queryToMap(String query){
		Map<String,String>result=new HashMap<String,String>();
		for(String param:query.split("&")){
			String pair[]=param.split("=");
			if(pair.length>1){
				result.put(pair[0],pair[1]);
			}else{
				result.put(pair[0],"");
			}
		}
		return result;
	}
	public static void main(String[]argumentos){
		try{
			utilidades.General.ruta=".\\";
			org.apache.commons.io.IOUtils txtx=null;
			if(hash==null){
				hash=new HashMap<String,Ejecutable>();
				hash.put("i",new Indice());
				hash.put("s",new Subir());
				hash.put("c",new Control());
				hash.put("a",new Actual());
			}
			HttpServer servidor=HttpServer.create(new InetSocketAddress(8383),0);
			servidor.createContext("/d",new HttpHandler(){
				public void handle(HttpExchange httpExchange)throws IOException{
					try{
						Map<String,String>parametros=Servidor.queryToMap(httpExchange.getRequestURI().getQuery());
						utilidades.General.imprimir("Parámetro O:"+parametros.get("o"));
						Servidor.writeResponse(httpExchange,Servidor.hash.get(parametros.get("o")).ejecutar(httpExchange,parametros));
					}catch(Throwable t){
						t.printStackTrace();
						utilidades.General.imprimir(t);
						Servidor.writeResponse(httpExchange,"error".getBytes());
					}
				}
			});
			servidor.createContext("/",new HttpHandler(){
				public void handle(HttpExchange httpExchange)throws IOException{
					try{
						Servidor.writeResponse(httpExchange,archivo((httpExchange.getRequestURI()+"").replace("/","")));
					}catch(Throwable t){
						t.printStackTrace();
						utilidades.General.imprimir(t);
						Servidor.writeResponse(httpExchange,"error".getBytes());
					}
				}
			});
			servidor.start();
			System.out.println("Esperando peticiones");
		}catch(Throwable t){
			t.printStackTrace();
		}
	}
	public static void abrir(byte[]datos)throws Exception{
		Sequence sq=MidiSystem.getSequence(new ByteArrayInputStream(datos));
		if(sx!=null){
			try{
				sx.stop();
			}catch(Exception e){
			}
		}
		if(sx==null){
			MidiDevice.Info[]ifs=MidiSystem.getMidiDeviceInfo();
			MidiDevice midiDevice=MidiSystem.getMidiDevice(ifs[0]);
			midiDevice.open();
			Receiver receiver=midiDevice.getReceiver();
			sx=MidiSystem.getSequencer(false);
			sx.getTransmitter().setReceiver(receiver);
			sx.setSequence(sq);
			sx.open();
			Escribir.escribir("Actual",sq);
		}
	}
	public static long getTime()throws Exception{
		return Math.round(sx.getMicrosecondPosition()/1000.);
	}
	public static void setTime(long t)throws Exception{
		sx.setMicrosecondPosition(t*1000);
		tini=(new Date()).getTime();
	}
	public static float getRate()throws Exception{
		return sx.getTempoFactor();
	}
	public static void setRate(float r)throws Exception{
		sx.setTempoFactor(r);
		tini=(new Date()).getTime();
	}
	public static void play()throws Exception{
		sx.start();
		tini=(new Date()).getTime();
	}
	public static void stop()throws Exception{
		sx.stop();
		tini=(new Date()).getTime();
	}
	public static long getLength()throws Exception{
		return sx.getMicrosecondLength();
	}
}