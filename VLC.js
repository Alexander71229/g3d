function VLC(o){
  this.getTime=function(){
  	return o.input.time;
  }
  this.setTime=function(t){
  	o.input.time=t;
  }
  this.getRate=function(){
  	return o.input.rate;
  }
  this.setRate=function(r){
  	o.input.rate=r;
  }
  this.play=function(){
  	o.playlist.play();
  }
  this.stop=function(){
  	o.playlist.togglePause();
  }
  this.getLength=function(){
  	o.input.length;
  }
}